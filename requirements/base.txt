# Bleeding edge Django
django<1.8

# Configuration
django-configurations>=0.8
django-secure>=1.0

# Your custom requirements go here
pyyaml<3.11
bioblend